#![forbid(unsafe_code)]
#![warn(clippy::unwrap_used)]
#![warn(clippy::todo)]
#![warn(clippy::dbg_macro)]

use std::{
    collections::BTreeMap,
    fs::File,
    io::{Read, Write},
    path::Path,
};

use api::Comic;
use egui_extras::RetainedImage;
use log::trace;

pub mod api;

#[derive(Debug, Default, serde::Serialize, serde::Deserialize)]
pub struct Metadata {
    pub comics: BTreeMap<u64, Comic>,
}

#[derive(Debug, thiserror::Error)]
pub enum ImageLoadError {
    #[error("Url format is not accepted: {0}")]
    InvalidUrl(String),
    #[error("Failed to download image: {0}")]
    Download(reqwest::Error),
    #[error("Failed to cache downloaded image: {0}")]
    Cache(std::io::Error),
    #[error("Failed to decode image: {0}")]
    Decode(String),
}

impl Metadata {
    pub fn load<P: AsRef<Path>>(path: P) -> Result<Self, Box<dyn std::error::Error>> {
        let mut f = File::open(path)?;
        let mut buf = String::new();
        f.read_to_string(&mut buf)?;
        Ok(serde_json::from_str(&buf)?)
    }

    pub fn save<P: AsRef<Path>>(&self, path: P) -> Result<(), Box<dyn std::error::Error>> {
        let mut f = File::create(path)?;
        let buf = serde_json::to_string(&self)?;
        f.write_all(buf.as_bytes())?;
        Ok(())
    }
}

pub async fn load_and_cache_image<P: AsRef<Path>>(
    url: &str,
    cache_dir: P,
) -> Result<RetainedImage, ImageLoadError> {
    let filename = url
        .rsplit_once('/')
        .ok_or_else(|| ImageLoadError::InvalidUrl(url.to_string()))?
        .1;
    let filepath = cache_dir.as_ref().join(filename);
    let bytes = if let Ok(bytes) = tokio::fs::read(&filepath).await {
        trace!("Loaded image from local path {}", filepath.display());
        bytes
    } else {
        let bytes = download_image(url).await?;
        tokio::fs::write(&filepath, bytes.as_slice())
            .await
            .map_err(ImageLoadError::Cache)?;
        trace!("Downloaded image from {url}");
        bytes
    };
    RetainedImage::from_image_bytes(filename, bytes.as_ref()).map_err(ImageLoadError::Decode)
}

async fn download_image(url: &str) -> Result<Vec<u8>, ImageLoadError> {
    let resp = reqwest::get(url).await.map_err(ImageLoadError::Download)?;
    let bytes = resp.bytes().await.map_err(ImageLoadError::Download)?;
    Ok(bytes.to_vec())
}
