#![forbid(unsafe_code)]
#![warn(clippy::unwrap_used)]
#![warn(clippy::todo)]
#![warn(clippy::dbg_macro)]
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

mod logging;

use std::sync::mpsc::Sender;
use std::{collections::BTreeMap, time::Duration};
use std::{
    collections::{BTreeSet, VecDeque},
    sync::mpsc::{self, Receiver},
};

use eframe::egui::RichText;
use eframe::epaint::Color32;
use eframe::{
    egui::{self, DragValue, Layout, ScrollArea, Ui},
    emath::{Align, NumExt},
    App,
};
use egui_extras::RetainedImage;
use log::error;
use logging::LogEntry;
use tokio::task::JoinHandle;
use xkcd_viewer::{
    api::{Comic, ComicId},
    load_and_cache_image, ImageLoadError, Metadata,
};

#[tokio::main]
async fn main() -> Result<(), eframe::Error> {
    log::set_logger(&logging::LOGGER).expect("Failed to set logger");
    log::set_max_level(log::LevelFilter::Trace);
    let options = eframe::NativeOptions::default();
    eframe::run_native(
        env!("CARGO_PKG_NAME"),
        options,
        Box::new(|_cc| Box::<Application>::default()),
    )
}

pub struct Application {
    metadata: BTreeMap<ComicId, Result<Comic, reqwest::Error>>,
    images: BTreeMap<String, Result<RetainedImage, ImageLoadError>>,
    lowercase_comics: BTreeMap<u64, Comic>,
    cell_size: f32,
    search: String,
    ignore_case: bool,
    show_about_window: bool,
    show_logs_window: bool,
    filtered_comics: Option<Vec<u64>>,
    downloads: BTreeMap<DownloadKey, JoinHandle<()>>,
    download_sender: Sender<DownloadResult>,
    download_receiver: Receiver<DownloadResult>,
    logs: VecDeque<LogEntry>,
    log_receiver: Receiver<LogEntry>,
    log_filter: BTreeSet<log::Level>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum DownloadKey {
    Metadata(ComicId),
    Image(String),
}

pub enum DownloadResult {
    Metadata(ComicId, Result<Comic, reqwest::Error>),
    Image(String, Result<RetainedImage, ImageLoadError>),
}

impl Default for Application {
    fn default() -> Self {
        let metadata = dirs::data_dir()
            .and_then(|data_dir| {
                let metadata_path = data_dir.join(env!("CARGO_PKG_NAME")).join("comics.json");
                Metadata::load(&metadata_path).ok()
            })
            .unwrap_or_default();
        let Metadata { comics } = metadata;
        let metadata = comics
            .into_iter()
            .map(|(key, value)| (ComicId::Number(key), Ok(value)))
            .collect();
        let (log_sender, log_receiver) = mpsc::channel();
        *logging::SENDER.lock().expect("Failed to initialize logger") = Some(log_sender);
        let log_filter = log::Level::iter().collect();
        let (download_sender, download_receiver) = mpsc::channel();
        Self {
            metadata,
            images: Default::default(),
            lowercase_comics: Default::default(),
            cell_size: 200.0,
            search: Default::default(),
            ignore_case: false,
            filtered_comics: Default::default(),
            downloads: Default::default(),
            logs: Default::default(),
            log_receiver,
            log_filter,
            show_about_window: false,
            show_logs_window: false,
            download_sender,
            download_receiver,
        }
    }
}

impl Drop for Application {
    fn drop(&mut self) {
        if let Some(data_path) = dirs::data_dir().and_then(|data_dir| {
            let app_data_dir = data_dir.join(env!("CARGO_PKG_NAME"));
            std::fs::create_dir_all(&app_data_dir).ok()?;
            Some(app_data_dir)
        }) {
            let first = self
                .metadata
                .keys()
                .copied()
                .next()
                .unwrap_or(ComicId::Latest);
            let comics = self
                .metadata
                .split_off(&first)
                .into_iter()
                .flat_map(|(key, value)| {
                    let ComicId::Number(num) = key else {
                    None?
                };
                    Some((num, value.ok()?))
                })
                .collect();
            _ = Metadata { comics }.save(&data_path.join("comics.json"));
        }
    }
}

impl App for Application {
    fn update(&mut self, ctx: &eframe::egui::Context, _frame: &mut eframe::Frame) {
        self.poll();
        if self.downloads_in_progress() > 0 {
            ctx.request_repaint_after(Duration::from_secs_f64(1.0));
        }
        egui::TopBottomPanel::bottom("status bar").show(ctx, |ui| {
            ui.horizontal(|ui| {
                egui::global_dark_light_mode_switch(ui);
                if ui.button("ℹ").on_hover_text("About").clicked() {
                    self.show_about_window ^= true;
                }
                if ui.button("📄").on_hover_text("Logs").clicked() {
                    self.show_logs_window ^= true;
                }
                self.show_about_window(ui);
                self.show_logs_window(ui);
            });
        });
        egui::CentralPanel::default().show(ctx, |ui| {
            self.display_comics(ui);
        });
    }
}

impl Application {
    fn poll(&mut self) {
        self.poll_metadata();
        self.update_filter();
    }

    fn poll_metadata(&mut self) {
        if self.latest_number().is_none() {
            self.downloads
                .entry(DownloadKey::Metadata(ComicId::Latest))
                .or_insert_with(|| {
                    let sender = self.download_sender.clone();
                    tokio::spawn(async move {
                        let id = ComicId::Latest;
                        let comic = Comic::fetch(id).await;
                        _ = sender.send(DownloadResult::Metadata(id, comic));
                    })
                });
        }
        while let Ok(download) = self.download_receiver.try_recv() {
            match download {
                DownloadResult::Metadata(key, result) => {
                    let value = result.map_err(|err| {
                        error!("Failed to load comic metadata: {err}");
                        err
                    });
                    self.metadata.insert(key, value);
                }
                DownloadResult::Image(key, result) => {
                    let value = result.map_err(|err| {
                        error!("Failed to load image: {err}");
                        err
                    });
                    self.images.insert(key, value);
                }
            }
        }
        self.downloads.retain(|_, handle| !handle.is_finished());
    }

    fn download_more_metadata(&mut self) {
        if self.downloads_in_progress() > 0 {
            return;
        }

        let Some(all_numbers) = self.latest_number().map(|latest| 1..=latest) else {
            return;
        };
        let missing_numbers: Vec<_> = all_numbers
            .filter(|&num| {
                self.metadata.get(&ComicId::Number(num)).is_none()
                    && !self
                        .downloads
                        .contains_key(&DownloadKey::Metadata(ComicId::Number(num)))
            })
            .take(100)
            .collect();
        for number in missing_numbers {
            let id = ComicId::Number(number);
            let sender = self.download_sender.clone();
            self.downloads
                .entry(DownloadKey::Metadata(id))
                .or_insert_with(|| {
                    tokio::spawn(async move {
                        let comic = Comic::fetch(id).await;
                        _ = sender.send(DownloadResult::Metadata(id, comic));
                    })
                });
        }
    }

    fn downloads_in_progress(&self) -> usize {
        self.downloads.len()
    }

    fn latest_number(&self) -> Option<u64> {
        self.metadata
            .get(&ComicId::Latest)
            .and_then(|c| Some(c.as_ref().ok()?.num))
    }

    fn update_filter(&mut self) {
        let Some(latest) = self.latest_number() else {
            return;
        };
        self.filtered_comics.get_or_insert_with(|| {
            let mut search = self.search.clone();
            let all_comics = 1..=latest;
            if search.is_empty() {
                return all_comics.collect();
            }
            if self.ignore_case {
                search = search.to_lowercase();
            }
            all_comics
                .filter_map(|num| {
                    let comic = self.metadata.get(&ComicId::Number(num))?.as_ref().ok()?;
                    let comic = if self.ignore_case {
                        self.lowercase_comics
                            .entry(num)
                            .or_insert_with(|| comic.to_lowercase())
                    } else {
                        comic
                    };
                    comic.contains(&search).then_some(num)
                })
                .collect()
        });
    }

    fn display_comics(&mut self, ui: &mut Ui) {
        if self.filtered_comics.is_none() {
            ui.centered_and_justified(|ui| {
                ui.spinner();
            });
            return;
        }
        let min_height = 150.0;
        let margin = 5.0;
        let max_height = ui.available_size().max_elem().at_least(min_height);
        ui.spacing_mut().item_spacing = [margin, margin].into();
        ui.horizontal(|ui| {
            let download_more = ui
                .add_enabled(self.downloads_in_progress() == 0, |ui: &mut Ui| {
                    let downloaded = self.metadata.len();
                    ui.button(format!("Download more ({downloaded})"))
                        .on_hover_text("Download missing comic metadata")
                })
                .clicked();
            if download_more {
                self.download_more_metadata();
            }
            ui.label("Size");
            ui.add(DragValue::new(&mut self.cell_size).clamp_range(min_height..=max_height));
            ui.label("Search");
            let mut changed = ui.text_edit_singleline(&mut self.search).changed();
            changed |= ui.checkbox(&mut self.ignore_case, "Ignore case").changed();
            if changed {
                self.filtered_comics.take();
            }
            if !self.search.is_empty() {
                let matches = self
                    .filtered_comics
                    .as_deref()
                    .map(|v| v.len())
                    .unwrap_or_default();
                ui.label(format!("{matches} matches"));
            }
        });
        let height = self.cell_size.clamp(min_height, max_height);
        let width = height;
        let items_per_row = ((ui.available_width() / (width + margin)) as usize).at_least(1);
        let comic_numbers = self.filtered_comics.clone().unwrap_or_default();
        let total_rows = comic_numbers.len() / items_per_row;
        ScrollArea::vertical().show_rows(ui, height, total_rows, |ui, rows| {
            ui.allocate_space([ui.available_width(), 0.0].into());
            ui.with_layout(
                Layout::left_to_right(Align::TOP).with_main_wrap(true),
                |ui| {
                    let visible_comics = comic_numbers
                        .iter()
                        .rev()
                        .copied()
                        .skip(rows.start * items_per_row)
                        .take((rows.len() + 1) * items_per_row);
                    for comic_number in visible_comics {
                        ui.allocate_ui([height, height].into(), |ui| {
                            ui.vertical(|ui| {
                                self.display_comic(ui, comic_number);
                            });
                        });
                    }
                },
            );
        });
    }

    fn display_comic(&mut self, ui: &mut Ui, number: u64) {
        ui.group(|ui| {
            ui.strong(format!("#{number}"));
            ui.with_layout(Layout::bottom_up(Align::Center), |ui| {
                let comic = self.metadata.get(&ComicId::Number(number));
                match comic {
                    Some(Ok(comic)) => {
                        ui.label(comic.title.as_ref())
                            .on_hover_text(comic.alt.as_ref());
                        ui.centered_and_justified(|ui| {
                            let available_size = ui.available_size();
                            match self.images.get(comic.img.as_ref()) {
                                Some(Ok(image)) => {
                                    image
                                        .show_max_size(ui, available_size)
                                        .on_hover_text(comic.alt.as_ref());
                                }
                                Some(Err(err)) => {
                                    ui.centered_and_justified(|ui| {
                                        ui.heading(RichText::new("⚠").color(Color32::RED))
                                            .on_hover_text(err.to_string());
                                    });
                                }
                                _ => {
                                    let Some(data_dir) = dirs::data_dir() else {
                                                                return;
                                                            };
                                    let img_dir = data_dir.join(env!("CARGO_PKG_NAME")).join("img");
                                    let Ok(_) = std::fs::create_dir_all(&img_dir) else {
                                                                return;
                                                            };
                                    let url = comic.img.to_string();
                                    let sender = self.download_sender.clone();
                                    self.downloads
                                        .entry(DownloadKey::Image(url.clone()))
                                        .or_insert_with(|| {
                                            tokio::spawn(async move {
                                                let result =
                                                    load_and_cache_image(url.as_str(), img_dir)
                                                        .await;
                                                _ = sender.send(DownloadResult::Image(url, result));
                                            })
                                        });
                                    ui.centered_and_justified(|ui| {
                                        ui.spinner();
                                    });
                                }
                            }
                        });
                    }
                    Some(Err(err)) => {
                        ui.centered_and_justified(|ui| {
                            ui.heading(RichText::new("⚠").color(Color32::RED))
                                .on_hover_text(err.to_string());
                        });
                    }
                    _ => {
                        let id = ComicId::Number(number);
                        let sender = self.download_sender.clone();
                        self.downloads
                            .entry(DownloadKey::Metadata(id))
                            .or_insert_with(|| {
                                tokio::spawn(async move {
                                    let result = Comic::fetch(id).await;
                                    _ = sender.send(DownloadResult::Metadata(id, result));
                                })
                            });
                        ui.centered_and_justified(|ui| {
                            ui.spinner();
                        });
                    }
                }
            });
        });
    }

    fn show_about_window(&mut self, ui: &mut egui::Ui) {
        egui::Window::new("About")
            .open(&mut self.show_about_window)
            .show(ui.ctx(), |ui| {
                ui.heading(format!(
                    "{} - {}",
                    env!("CARGO_PKG_NAME"),
                    env!("CARGO_PKG_VERSION")
                ));
                ui.label(env!("CARGO_PKG_DESCRIPTION"));
                ui.label(format!("Authors: {}", env!("CARGO_PKG_AUTHORS")));
                ui.label(format!("⚖ {}", env!("CARGO_PKG_LICENSE")));
                ui.add(egui::Hyperlink::from_label_and_url(
                    "Repository",
                    env!("CARGO_PKG_REPOSITORY"),
                ));
            });
    }

    fn show_logs_window(&mut self, ui: &mut egui::Ui) {
        let max_width = ui.ctx().screen_rect().width() * 0.8;
        let max_height = ui.ctx().screen_rect().height() * 0.8;
        egui::Window::new("Logs")
            .open(&mut self.show_logs_window)
            .show(ui.ctx(), |ui| {
                while let Ok(s) = self.log_receiver.try_recv() {
                    self.logs.push_front(s);
                }
                self.logs.truncate(10000);
                ui.collapsing("Filter", |ui| {
                    self.log_filter = log::Level::iter()
                        .filter_map(|level| {
                            let mut enabled = self.log_filter.contains(&level);
                            ui.checkbox(&mut enabled, level.as_str());
                            enabled.then_some(level)
                        })
                        .collect();
                });
                let total_logs = self.logs.len();
                let filtered_log_indices: Vec<_> = (0..total_logs)
                    .filter_map(|i| {
                        self.log_filter
                            .contains(&self.logs.get(i)?.level)
                            .then_some(i)
                    })
                    .collect();
                let row_height = ui.text_style_height(&egui::TextStyle::Body);
                let total_rows = filtered_log_indices.len();
                egui::ScrollArea::both()
                    .max_height(max_height)
                    .max_width(max_width)
                    .show_rows(ui, row_height, total_rows, |ui, row_range| {
                        ui.allocate_space([300.0, 0.0].into());
                        let logs_to_display = row_range.filter_map(|i| {
                            filtered_log_indices.get(i).and_then(|i| self.logs.get(*i))
                        });
                        for entry in logs_to_display {
                            ui.horizontal(|ui| {
                                let level = entry.level;
                                let level_icon = match level {
                                    log::Level::Error => "❎",
                                    log::Level::Warn => "⚠",
                                    log::Level::Info => "ℹ",
                                    log::Level::Debug => "🐛",
                                    log::Level::Trace => "📊",
                                };
                                let level_color = match level {
                                    log::Level::Error => Color32::RED,
                                    log::Level::Warn => Color32::YELLOW,
                                    log::Level::Info => Color32::BLUE,
                                    log::Level::Debug => Color32::DARK_GREEN,
                                    log::Level::Trace => Color32::GRAY,
                                };
                                let message = entry.message.as_str();
                                ui.monospace(RichText::new(level_icon).color(level_color))
                                    .on_hover_text(level.to_string());
                                ui.monospace(entry.timestamp.format("%H:%M:%S.%3f").to_string())
                                    .on_hover_text(entry.timestamp.to_rfc3339());
                                ui.monospace(message).on_hover_text(message);
                            });
                        }
                    });
            });
    }
}
