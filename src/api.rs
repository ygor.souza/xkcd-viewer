use log::trace;

static APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct Comic {
    pub month: Box<str>,
    pub num: u64,
    pub link: Box<str>,
    pub year: Box<str>,
    pub news: Box<str>,
    pub safe_title: Box<str>,
    pub transcript: Box<str>,
    pub alt: Box<str>,
    pub img: Box<str>,
    pub title: Box<str>,
    pub day: Box<str>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ComicId {
    Latest,
    Number(u64),
}

impl Comic {
    pub async fn fetch(id: ComicId) -> Result<Self, reqwest::Error> {
        let url = match id {
            ComicId::Latest => "https://xkcd.com/info.0.json".to_owned(),
            ComicId::Number(num) => format!("https://xkcd.com/{num}/info.0.json"),
        };
        let client = app_client()?;
        let request = client.get(&url).build()?;
        let resp = client.execute(request).await?.error_for_status()?;
        trace!("Downloaded comic metadata from {url}");
        resp.json::<Self>().await
    }

    pub async fn image_data(&self) -> Result<Box<[u8]>, reqwest::Error> {
        let client = app_client()?;
        let request = client.get(self.img.as_ref()).build()?;
        let resp = client.execute(request).await?;
        let bytes = resp.bytes().await?;
        Ok(bytes.to_vec().into())
    }

    pub fn contains(&self, search_term: &str) -> bool {
        self.title.contains(search_term)
            || self.alt.contains(search_term)
            || self.transcript.contains(search_term)
    }

    pub fn to_lowercase(&self) -> Self {
        let mut out = self.clone();
        out.title = out.title.to_lowercase().into();
        out.alt = out.alt.to_lowercase().into();
        out.transcript = out.transcript.to_lowercase().into();
        out
    }
}

fn app_client() -> Result<reqwest::Client, reqwest::Error> {
    reqwest::ClientBuilder::new()
        .user_agent(APP_USER_AGENT)
        .build()
}
