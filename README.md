# xkcd viewer

GUI application that downloads and displays xkcd comics, written in Rust.

It uses the JSON interface described in <https://xkcd.com/about/> to download
the metadata of the comics and then stores it in a local file that is read at
startup. It also downloads the images from the URLs listed in the metadata and
saves them to local files to avoid downloading them again every time.

This project is being developed as a learning exercise, to study techiniques
that allow reconciling async code and immediate UI.

## Using this application

Binary executables are not provided for the moment, so the application must be
compiled from source:

- Install the Rust compiler and tools following the instructions in
  https://www.rust-lang.org/tools/install
- Open a command prompt in the project's folder and run the command
`cargo build --release`. This will create an executable file in the
`target/release/` subdirectory, which can be copied to another folder and used
to launch the application.

## Disclaimer

This project has no affiliation with the <https://xkcd.com> website or its owner.
